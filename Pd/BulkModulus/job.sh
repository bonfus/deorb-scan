#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=4
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 07:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J Pd_B

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=4

for sc in {90..110}
do
mkdir -p $sc
fsc=$(awk -v num=$sc 'BEGIN{print (num/100.0)^(1/3)}')
cat elk.in.template | sed "s/XSC/$fsc/" > $sc/elk.in
cd $sc
mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
cd ..
done
