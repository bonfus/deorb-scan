# coding: utf-8
import pandas as pd

tab3 = pd.read_csv('tab3.csv',header=0,sep=r'\s*,\s*',skipinitialspace=True)
tab4 = pd.read_csv('tab4.csv',header=0,sep=r'\s*,\s*',skipinitialspace=True)

tab4.Solid = tab4.Solid.str.strip()
tab3.Solid = tab3.Solid.str.strip()

data = pd.merge(tab3,tab4,on='Solid')

results = []
for s in data.Solid:
    try:
        elk=pd.read_json('{}/BulkModulus/eos.json'.format(s))
    except:
        continue
    elk.rename(columns={'a0':'Elk-a0','B0':'Elk-B0'},inplace=True)
    results.append(elk)

results = pd.concat(results)


data = data.merge(results,on='Solid')


import matplotlib.pyplot as plt
import numpy as np

fig,ax = plt.subplots()

ax.plot(data['Exp A0'],data['Elk-a0'],  'o', label='Elk (rSCAN)', mfc='none')
ax.plot(data['Exp A0'],data['SCAN A0'], 'o', label='SCAN (PRB 98 115161)',        mfc='none')
ax.plot(data['Exp A0'],data['SCANL A0'],'o', label='SCANL (PRB 98 115161)',       mfc='none')

ax.plot(np.linspace(data['Exp A0'].min(),data['Exp A0'].max()), 
        np.linspace(data['Exp A0'].min(),data['Exp A0'].max()),
        color='k')

basev = data['Exp A0'].min()
i=0
for s,a0 in data[['Solid' , 'Exp A0']].sort_values(by='Exp A0').values:
    ax.annotate(s,(a0, basev + 0.02 * basev * (i%3)),fontsize=8)
    i+=1

ax.legend()
ax.set_xlabel('Experimental A0 [Ang.]')
ax.set_ylabel('Computed A0 [Ang.]')

fig.tight_layout()
fig.savefig('LatticeParameters.pdf')

fig,ax = plt.subplots()

ax.plot(data['B0 Expt.'],data['Elk-B0'],  'o', label='Elk (rSCAN)', mfc='none')
ax.plot(data['B0 Expt.'],data['B0 SCAN'], 'o', label='SCAN (PRB 98 115161)',        mfc='none')
ax.plot(data['B0 Expt.'],data['B0 SCANL'],'o', label='SCANL (PRB 98 115161)',       mfc='none')

ax.plot(np.linspace(data['B0 Expt.'].min(),data['B0 Expt.'].max()), 
        np.linspace(data['B0 Expt.'].min(),data['B0 Expt.'].max()),
        color='k')

basev = data['B0 Expt.'].min()
i=0
for s,a0 in data[['Solid' , 'B0 Expt.']].sort_values(by='B0 Expt.').values:
    ax.annotate(s,(a0, basev + 3*basev * (i%4)),fontsize=8)
    i+=1

ax.legend()
ax.set_xlabel('Experimental B0 [GPa]')
ax.set_ylabel('Computed B0 [GPa]')
fig.tight_layout()
fig.savefig('BulkModulus.pdf')
