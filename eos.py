import json, os, glob
import matplotlib.pyplot as plt
import numpy as np


def sjeos(v, alpha, beta, gamma, omega):
    return alpha * (1/v) + beta * (1/v)**(2./3.) + gamma * (1/v)**(1./3.) + omega

def bulk_mod(data):
    from scipy.optimize import curve_fit

    x = data[:,0] * 0.14818471 # a.u.^3 -> ang ^3
    y = data[:,1] * 27.211387 # eV

    r, cov = curve_fit(sjeos, x, y)

    alphas,betas,gammas,omegas = r

    #V0 = ((-betas + np.sqrt(betas**2 - 3*alphas*gammas))/(gammas))**3
    V0 = ((-betas - np.sqrt(betas**2 - 3*alphas*gammas))/(gammas))**3

    alpha = alphas/V0
    beta  = betas / V0**(2/3)
    gamma = gammas / V0**(1/3)

    B0 = ( 18*alpha + 10*beta + 4*gamma)/(9*V0)

    B0 *= 160.21766 # eV/(angstrom^3) -> GPa

    return x,y,r,V0,B0

def get_info():
    solid_name = os.path.split(os.path.split(os.getcwd())[0])[1]
    with open('../../info.json','r') as f:
        info = json.load(f)

    for e in info:
        if e['solid'] == solid_name:
            return e


def collect_cubic():
    data = []
    for sc in range(90,111):
        if not(os.path.isfile(str(sc)+'/INFO.OUT')):
            print('Something wrong!!!')
            break

        with open(str(sc)+'/INFO.OUT','r') as f:
            for line in f.readlines():
                if 'unit cell volume' in line.lower():
                    volume = float(line.split(':')[1])
        with open(str(sc)+'/TOTENERGY.OUT','r') as f:
            E = float(f.readlines()[-1])

        data.append((volume, E))
    return data

def collect_hcp():
    data = []
    for sc in range(90,111):
        datasc = []
        for sdir in glob.glob(str(sc)+'/*'):

            if not(os.path.isdir(sdir)):
                continue
            if not(os.path.isfile(sdir+'/INFO.OUT')):
                print('Something wrong!!!')
                break

            with open(sdir+'/INFO.OUT','r') as f:
                lines = f.readlines()
                for i, line in enumerate(lines):
                    if 'unit cell volume' in line.lower():
                        volume = float(line.split(':')[1])
                    if 'Lattice vectors' in line:
                        a = np.linalg.norm([float(x) for x in lines[i+1].strip().split()])
                        b = np.linalg.norm([float(x) for x in lines[i+2].strip().split()])
                        c = np.linalg.norm([float(x) for x in lines[i+3].strip().split()])

            with open(sdir+'/TOTENERGY.OUT','r') as f:
                E = float(f.readlines()[-1])
            datasc.append((volume, (a,b,c), E))
        print(datasc)
        # Check volume
        assert(np.std([V for V,_,_ in datasc]) < 0.01) # a.u.^3
        volume = np.average([V for V,_,_ in datasc])
        # Now fit
        x = np.array([c/a for _,(a,_,c),_ in datasc])
        y = np.array([e for _,(_,_,_),e in datasc])
        a,b,c = np.polyfit(x,y,2)
        #plt.scatter(x,y)
        #xx = np.linspace(np.min(x), np.max(x))
        #plt.plot(xx, a * xx**2 + b * xx + c)
        #plt.show()

        Emin = c-(b**2)/(4*a)
        c_a_min = -b/(2*a)

        data.append((volume, Emin, c_a_min))
    return data


info = get_info()

if info['structure'] == 'HCP':
    data = np.array(collect_hcp())
else:
    data = np.array(collect_cubic())



x,y, r, V0, B0 = bulk_mod(data)

if info['structure'] == 'BCC':
    a0 = np.cbrt(V0*2)
elif info['structure'] == 'FCC':
    a0 = np.cbrt(V0*4)
elif info['structure'] == 'HCP':
    # linearly interpolate c/a

    c_a = np.interp(V0, x, data[:,2])

    # V = a* a* c * np.sqrt(3) / 2
    #    = a^3 (c/a) * np.sqrt(3) / 2
    a0 = np.cbrt(V0*2/(np.sqrt(3)*c_a))

with open('eos.json','w') as f:
    json.dump([{'Solid': info['solid'], 'a0':a0,'B0':B0}],f)

f,ax = plt.subplots()
ax.set_title(info['solid'])
ax.scatter(x,y)
ax.plot(x,sjeos(x,*r), label='SJEOS')
ax.annotate('a$_0$: {:.3f} Ang.\nV$_0$: {:.3f} Ang^3\nB$_0$: {:.2f} GPa\n\na$_{{0}}^{{(exp)}}$: {:.3f} Ang.\nB$_{{0}}^{{(exp)}}$: {:.2f} GPa'.format(a0,V0,B0,info['exp_latt'], info['exp_B0']), (0.4,0.6),xycoords='figure fraction')
ax.set_ylabel('Energy (eV)')
ax.set_xlabel('Volume (Ang.^3)')
ax.legend()
f.tight_layout()
f.savefig('../../'+info['solid']+'.png')

