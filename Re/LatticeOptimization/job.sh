#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=4
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 25:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J Re_RLX

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=4


mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
