#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=16
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 3:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J Os_B

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=16

for sc in {104..104}
do
  ../../clean.sh
  mkdir -p $sc
  cd $sc

  for ac in {96..104..2}
  do
    if [ -d "$ac" ]; then
      continue
    fi
    mkdir -p $ac

    fsc=$(awk -v num=$sc 'BEGIN{print (num/100.0)^(1/3)}')
    asc=$(awk -v num=$ac 'BEGIN{print (num/100.0)^(1/2)}')
    csc=$(awk -v num=$ac 'BEGIN{print (1/(num/100.0))}')

    cat ../elk.in.template | sed -e "s/XSC/$fsc/"                                 -e "s/ASC/$asc/"                                 -e "s/CSC/$csc/"                                 > $ac/elk.in
    cd $ac
    cp ../STATE.OUT .
    cp ../EFERMI.OUT .
    # reuse density
    if [ $? -eq 0 ]; then sed -i 's/!tasks/tasks\n  1/g' elk.in; fi
    mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
    cp STATE.OUT ..
    cp EFERMI.OUT ..
    cd ..
  done
cd ..
done
