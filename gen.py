import json, os

A2Bohr = 1.8897262

settings = """
tasks
  {task}

latvopt
  {latvopt}

maxlatvstp
  200

vhighq
  .true.

rgkmax
  8.5

lradstp
  1

isgkmax
 -2

gmaxvr
 -8

! no limit on potential
epspot
 10

epsengy
 1d-8

xctype
  100 493 494

stype
  1

swidth
  0.001

spinpol
 .false.

spinorb
 .false.

nwrite
  2

sppath
  '/home/vol08/scarf498/elk-8.3.15/species/'

ngridk
  {ngridk}

autokpt
  {autokpt}

radkpt
  60
"""

lattice = {'bcc':
"""
avec
  0.5  0.5 -0.5
  0.5 -0.5  0.5
 -0.5  0.5  0.5
""",
'fcc':
"""
avec
  0.5  0.5  0.0
  0.5  0.0  0.5
  0.0  0.5  0.5
""",
'hcp':
"""
avec
  0.8660254038   -0.5    0.0
  0.0             1.0    0.0
  0.0             0.0    1.633
"""
}

details = {
'bcc': """
scale
  {explatt}

scale1
  {scale}

scale2
  {scale}

scale3
  {scale}

atoms
  1                                   : nspecies
  '{atom}.in'                             : spfname
  1                                   : natoms
  0.0  0.0  0.0    0.0  0.0  0.0      : atposl, bfcmt
""",
'fcc': """
scale
  {explatt}

scale1
  {scale}

scale2
  {scale}

scale3
  {scale}

atoms
  1                                   : nspecies
  '{atom}.in'                             : spfname
  1                                   : natoms
  0.0  0.0  0.0    0.0  0.0  0.0      : atposl, bfcmt
""",
'hcp': """
scale
  {explatt}

scale1
  {scalea}

scale2
  {scalea}

scale3
  {scalec}
atoms
  1
  '{atom}.in'
  2
  0.3333333333   0.6666666667   0.25
  0.6666666667   0.3333333333   0.75
"""
}

JOBBULK="""#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 13:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J {jobname}

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=1

for sc in {{90..110}}
do
mkdir -p $sc
fsc=$(awk -v num=$sc 'BEGIN{{print (num/100.0)^(1/3)}}')
cat elk.in.template | sed "s/XSC/$fsc/" > $sc/elk.in
cd $sc
mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
cd ..
done
"""


JOBBULKHCP="""#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 13:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J {jobname}

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=1

for sc in {{90..110}}
do
  mkdir -p $sc
  cd $sc

  for ac in {{96..104..2}}
  do
    mkdir -p $ac

    fsc=$(awk -v num=$sc 'BEGIN{{print (num/100.0)^(1/3)}}')
    asc=$(awk -v num=$ac 'BEGIN{{print (num/100.0)^(1/2)}}')
    csc=$(awk -v num=$ac 'BEGIN{{print (1/(num/100.0))}}')

    cat ../elk.in.template | sed -e "s/XSC/$fsc/" \
                                -e "s/ASC/$asc/" \
                                -e "s/CSC/$csc/" \
                                > $ac/elk.in
    cd $ac
    mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
    cd ..
  done
cd ..
done
"""


JOBRLX="""#!/bin/bash
#SBATCH -N 1
######SBATCH --exclusive
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
####SBATCH -C [scarf18,scarf19]
#SBATCH -t 05:30:00
####SBATCH --mem=150G
#SBATCH --exclude=cn581
#SBATCH -J {jobname}

module load intel/cce/18.0.3  intel/fce/18.0.3  intel/18.0.3   intel/mpi/18.0.3  intel/mkl/18.0.3
export OMP_STACKSIZE=100M
export OMP_NUM_THREADS=1


mpirun -np 1 $HOME/elk-8.3.15/src/elk > log.OUT
"""

with open('info.json','r') as f:
    info = json.load(f)

what = 'bulk_modulus'

if what == 'bulk_modulus':
    for structure in info:
        solid = structure['solid']
        lattype = structure['structure'].lower()
        explatt = structure['exp_latt']
        try:
            os.mkdir(solid)
        except:
            print('solid ' + solid + ' already done')
            continue
        os.mkdir(solid + '/LatticeOptimization')

        with open(solid + '/LatticeOptimization/elk.in', 'w') as f:
            f.write(settings.format(task=2, latvopt=1,
                                    autokpt='.true.',
                                    ngridk='1 1 1'))
            f.write(lattice[lattype])
            f.write(details[lattype].format(scale=1.0,
                                            scalea=1.0,scaleb=1.0,scalec=1.0,
                                            atom=solid,
                                            explatt=explatt*A2Bohr))
        with open(solid + '/LatticeOptimization/job.sh', 'w') as f:
            f.write(JOBRLX.format(jobname=(solid+'_RLX')))

# Gen Bulk modulus
if what == 'bulk_modulus':
    for structure in info:
        solid = structure['solid']
        lattype = structure['structure'].lower()
        explatt = structure['exp_latt']
        try:
            os.mkdir(solid + '/BulkModulus')
        except:
            print('Warning: solid ' + solid + ' already created')
            continue

        with open(solid + '/LatticeOptimization/GEOMETRY.OUT', 'r') as f:
            lattice_info = f.read().replace('scale\n 1.0', 'scale\n {scale}')\
                                    .replace('scale1\n 1.0', 'scale1\n {scalea}')\
                                    .replace('scale2\n 1.0', 'scale2\n {scaleb}')\
                                    .replace('scale3\n 1.0', 'scale3\n {scalec}')

        with open(solid + '/BulkModulus/elk.in.template', 'w') as f:
            f.write(settings.format(task=0, latvopt=0,
                                    autokpt='.false.',
                                    ngridk='17 17 17'))
            if lattype in ['fcc','bcc']:
                f.write(lattice_info.format(scale='XSC', scalea='1.0', scaleb='1.0', scalec='1.0'))
            elif lattype == 'hcp':
                f.write(lattice_info.format(scale='XSC', scalea='ASC', scaleb='ASC', scalec='CSC'))

        with open(solid + '/BulkModulus/job.sh', 'w') as f:
            if not lattype == 'hcp':
                f.write(JOBBULK.format(jobname=(solid+'_B')))
            else:
                f.write(JOBBULKHCP.format(jobname=(solid+'_B')))
