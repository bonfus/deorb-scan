# Tests for deorbitalized SCAN

Collection of scripts and data for testing rSCAN and the brand new implementation
of meta-GGA in Elk.

## Notes

* Converge against RMS change in KS potential is impossible.
* Most materials do not converge with SCAN, I had to use rSCAN (well known reasons).
* `rgkmax` sometimes must be reduced to avoid instabilities.

## Current settings

The lattice relaxation has been conducted with:

* `vhighq` and `rgkmax >= 8.0`,
* `radk=60`

The bulk modulus has been estimated with

* `vhighq` and `rgkmax >= 8.0`,
* `ngridk=17 17 17`,
* +/- 10% of equilibrium volume,
* MT radius reduced in order to keep the same value in all simulations.

## Results

Possibly repeat

* Rb


### Li - BCC
![Li](Li.png)

### Na - BCC
![Na](Na.png)

### K - BCC
![K](K.png)

### Rb - BCC
![Rb](Rb.png)

### Nb - BCC
![Nb](Nb.png)

### W - BCC
![W](W.png)

### Cu - FCC
![Cu](Cu.png)

### Ir - FCC
![Ir](Ir.png)

### Pd - FCC
![Pd](Pd.png)

### Pt - FCC
![Pt](Pt.png)

### Ag - FCC
![Ag](Ag.png)

### Au - FCC
![Au](Au.png)

